var arr1 = ['0e089d7ee981bec654acb4a678cef7e6', '7adefed65ed36cb257a5e44bc0ad9919', '64450e7b290f8abcfb070a27d5eaf202'];
var arr2 = [{
   "hashKey" : "7adefed65ed36cb257a5e44bc0ad9919",
   "updatedAt" : new Date("2020-02-02T07:56:44.731Z"),
   "sizeInBytes" : 127543,
   "storePath" : "arxiv/pdf/astro-ph0001011.pdf",
   "createdAt" : new Date("2020-02-02T07:56:44.731Z")
}, {
   "hashKey" : "0e089d7ee981bec654acb4a678cef7e6",
   "updatedAt" : new Date("2020-02-07T07:56:44.731Z"),
   "sizeInBytes" : 197563,
   "storePath" : "arxiv/pdf/astro-ph0001018.pdf",
   "createdAt" : new Date("2020-02-07T07:56:44.731Z")
}, {}, {
   "hashKey" : "64450e7b290f8abcfb070a27d5eaf202",
   "updatedAt" : new Date("2020-02-06T07:56:44.731Z"),
   "sizeInBytes" : 15705,
   "storePath" : "arxiv/pdf/astro-ph0001010.pdf",
   "createdAt" : new Date("2020-02-06T07:56:44.731Z")
}];


// arr2.sort();
var demoAr = [];
function demo(...args){

   // var ar = Array.from(args).filter(x=>true);
   for(const itr of ar)
   {
      demoAr.push(itr.hashKey);
      // console.log(itr.hashKey);
   }
   
   demoAr.sort((a,b)=> a-b);
   console.log(demoAr);
   // console.log(args.hashKey);
}
demo(...arr2);
// arr2.sort((a,b) => a.hashKey-b.hashKey);
// console.log(arr2);
// There is an array (arr1) containing hash value and a second array (arr2) contains array of objects. 
// Sort arr2 in the same order as arr1 using "hashKey". Come up with an optimized solution.

